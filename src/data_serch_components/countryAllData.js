import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";
import { baseUrl } from "./constants";
import { Table, Button, DatePicker } from "antd";
import Column from "antd/lib/table/Column";

const dateFormat = "YYYY-MM-DD";

function CountryAllData({ match }) {
  var [items, setItem] = useState(null);

  var country = match.params.Country;

  const retrive = () => {
    var Date1 = document.getElementById("date1").value + "T00:00:00Z";
    var Date2 = document.getElementById("date2").value;
    if (Date2 === "") {
      const url = `${baseUrl}/live/country/${country}/status/confirmed/date/${Date1}`;
      fetch(url)
        .then(function (response) {
          return response.json();
        })
        .then(function (data) {
          setItem(data);
        });
    } else {
      Date2 = Date2 + "T00:00:00Z";
      const url = `${baseUrl}/country/${country}?from=${Date1}&to=${Date2}`;
      fetch(url)
        .then(function (response) {
          return response.json();
        })
        .then(function (data) {
          setItem(data);
        });
    }
  };

  const fetchItem1 = useCallback(async () => {
    const url = `${baseUrl}/live/country/${country}`;
    const data = await (await fetch(url)).json();
    setItem(data);
  }, [country]);

  useEffect(() => {
    fetchItem1();
  }, [fetchItem1]);

  return (
    <div>
      From
      <DatePicker id="date1" format={dateFormat} />
      To
      <DatePicker id="date2" format={dateFormat} />
      <Button onClick={retrive}>Get Data</Button>
      <br />
      {/* <Button><Link to={`/confirmed/${country}`} >{country}</Link></Button> */}
      <Table dataSource={items}>
        <Column
          title="Date"
          dataIndex="Date"
          key="Date"
          sorter={(a, b) => a.Date - b.Date}
        />
        <Column
          title="Confirmed"
          dataIndex="Confirmed"
          key="Confirmed"
          sorter={(a, b) => a.Confirmed - b.Confirmed}
        />
        <Column title="Deaths" dataIndex="Deaths" key="Deaths" />
        <Column title="Recovered" dataIndex="Recovered" key="Recovered" />
        <Column title="Active" dataIndex="Active" key="Active" />
      </Table>
      <Link to={"/main"}>
        <button>Previous</button>
      </Link>
    </div>
  );
}

export default CountryAllData;
