import React, { Component } from "react";
import "antd/dist/antd.css";
import { Select } from "antd";
import { Link } from "react-router-dom";
import { baseUrl } from "./constants";
import { Table } from "antd";
import Column from "antd/lib/table/Column";

const { Option } = Select;

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      globaldata: {},
      Countries: [],
    };
  }

  async componentDidMount() {
    const url = `${baseUrl}/summary`;
    const responce = await fetch(url);
    const allData = await responce.json();
    this.setState({ globaldata: allData.Global, Countries: allData.Countries });
  }

  render() {
    return (
      <div className="container">
        {/* <h3>NewConfirmed : {this.state.globaldata.NewConfirmed}</h3>
        <h3>TotalConfirmed : {this.state.globaldata.TotalConfirmed}</h3>
        <h3>NewDeaths : {this.state.globaldata.NewDeaths}</h3>
        <h3>TotalDeaths : {this.state.globaldata.TotalDeaths}</h3>
        <h3>NewRecovered : {this.state.globaldata.NewRecovered}</h3>
        <h3>TotalRecovered : {this.state.globaldata.TotalRecovered}</h3> */}

        {/* for retriving live data for each country */}
        <Select
          size="large"
          showSearch
          defaultValue="Search country"
          style={{ width: 400 }}
          filterOption={(input, option) => option.children.indexOf(input) >= 0}
        >
          {this.state.Countries.map((country, i) => (
            <Option key={i} value={i}>
              <Link to={`/country/live/${i}`}>{country.Country}</Link>
            </Option>
          ))}
        </Select>
        <Table dataSource={this.state.Countries}>
          <Column
            title="Country"
            dataIndex="Country"
            key="Country"
            sorter={(a, b) => a.Country.localeCompare(b.Country)}
            render={(text) => <Link to={`/country/${text}`}>{text}</Link>}
          />
          <Column
            title="NewConfirmed"
            dataIndex="NewConfirmed"
            key="NewConfirmed"
          />
          <Column
            title="TotalConfirmed"
            dataIndex="TotalConfirmed"
            key="TotalConfirmed"
          />
          <Column title="NewDeaths" dataIndex="NewDeaths" key="NewDeaths" />
          <Column
            title="TotalDeaths"
            dataIndex="TotalDeaths"
            key="TotalDeaths"
          />
          <Column
            title="NewRecovered"
            dataIndex="NewRecovered"
            key="NewRecovered"
          />
          <Column
            title="TotalRecovered"
            dataIndex="TotalRecovered"
            key="TotalRecovered"
          />
          <Column title="Date" dataIndex="Date" key="Date" />
        </Table>
      </div>
    );
  }
}

export default Main;
