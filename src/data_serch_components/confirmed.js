import React, { useState, useEffect, useCallback } from "react";
import { Link } from "react-router-dom";
import { baseUrl } from "./constants";
import { Table, Button } from "antd";
import Column from "antd/lib/table/Column";

function ConfirmedData({ match }) {
  const [items, setItem] = useState(null);

  const name = match.params.Country;
  const fetchItem = useCallback(async () => {
    const url = `${baseUrl}/dayone/country/${name}/status/confirmed/live`;
    const data = await (await fetch(url)).json();
    console.log(data);
    setItem(data);
  }, [name]);

  useEffect(() => {
    fetchItem();
  }, [fetchItem]);

  return (
    <div>
      <Table dataSource={items}>
        <Column title="Date" dataIndex="Date" key="Date" />
        <Column
          title="Confirmed"
          dataIndex="Cases"
          key="Cases"
          sorter={(a, b) => a.Cases - b.Cases}
        />
        <Column title="Lat" dataIndex="Lat" key="Lat" />
        <Column title="Lon" dataIndex="Lon" key="Lon" />
        <Column title="Status" dataIndex="Status" key="Status" />
      </Table>
      <Link to={"/main"}>
        <Button>Previous</Button>
      </Link>
    </div>
  );
}

export default ConfirmedData;
