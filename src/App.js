import React from "react";
import "./App.css";
import Main from "./data_serch_components/Main";
// import DataDisplay from "./data_serch_components/DataDisplay";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import CountryAllData from "./data_serch_components/countryAllData";
import ConfirmedData from "./data_serch_components/confirmed";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={Main} />
          <Route path="/main" component={Main} />
          {/* <Route path="/country/live/:id" component={DataDisplay} /> */}
          <Route path="/country/:Country" component={CountryAllData} />
          <Route path="/confirmed/:Country" component={ConfirmedData} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
